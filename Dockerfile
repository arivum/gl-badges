FROM golang:1.12.6-alpine3.10 AS builder

COPY . /src
WORKDIR /src
ENV GO111MODULE=on

RUN apk add --update git && \
    go get -v && \
    go build


FROM alpine:3.10

RUN apk add --update ca-certificates bash
COPY --from=builder /src/gl-badges /usr/bin/gl-badges

WORKDIR /src

CMD gl-badges
