# GitLab Badges

Beautify your GitLab projects by adding custom badges, that are generated from a configuration file inside your repo. The badges are generated during a pipeline by applying the setup described in the next section.

## Setup

There are only three steps necessary to add custom badges.

1. Create an Access Token (`Profile` -> `Settings` -> `Access Token`) with scope `api` checked. Create an environment variable `API_KEY` with the generated token in your repositories' `Settings` -> `CI/CD` -> `Variables`.

2. Append a template include to your `.gitlab-ci.yml`
   ```yaml
    include:
      remote: https://gitlab.com/arivum/gl-badges/raw/master/Badges.gitlab-ci.yml

   ```
   **OR**
   
   Include following job definition if you need to install further binaries (e.g. if you use script output for badge generation)
   ```yaml
   gl_badges:
     stage: deploy
     image: registry.gitlab.com/arivum/gl-badges:latest
     variables:
       DOCKER_DRIVER: overlay2
       PROJECT: $CI_PROJECT_PATH
     script:
       # here you can install further applications e.g. apk add --update jq     
       - gl-badges
     only:
       refs:
         - branches
     except:
       variables:
         - $UPDATE_GL_BADGES_DISABLED

   ```
3. Create a `.gitlab-badges.yml` like this:
   ```yaml
   settings:
     style: flat-square
   badges:
     pipeline: null
     coverage: null
     sast:
       color: purple
     container_scanning: null
     dependency_scanning:
       color: red
     license:
       color: 007D7D
       link: https://gitlab.com/%{project_path}/blob/master/LICENSE
       label: license
       message: MIT
     generated:
       color: yellow
       # Counts files, output gets written to a yellow badge
       script: |
         ls -lah | wc -l

   ```
  
## Settings for .gitlab-badges.yml

There are a few predefined `badges` keys like `pipeline`, `coverage`, `sast`, `container_scanning` and `dependency_scanning` that are associated with special CI jobs.

The key `settings.style` lets you define either `flat-square` badges or the default `rounded` style.

For these jobs ensure to pass the generated report files to the final `gl_badges` job by defining artifacts the following way.

```yaml
artifacts:
  paths:
    - gl-dependency-scanning-report.json
  reports:
    dependency_scanning: gl-dependency-scanning-report.json
```

| **Badge name** | **Associated Job** | **Report file** |
| ---------- | -------------- | ----------- |
| pipeline   | whole pipeline  |             |
| coverage   | coverage test job |             |
| sast       | [sast](https://docs.gitlab.com/ee/user/application_security/sast/) | gl-sast-report.json |
| container_scanning | [container_scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/) | gl-container-scanning-report.json |
| dependency_scanning | [dependency_scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) | gl-dependency-scanning-report.json |


### Optional sub-keys for badges

All predefined badges do have a reduced set of sub-keys. The generation of the badge message is implemented already.

| **Key** | **Description** | **Optional sub-keys** |
| --- | ----------- | ---------------- |
| pipeline | Pipeline status badge | `link` |
| coverage | Coverage status badge | `link` |
| sast     | SAST vulnerability count | `link`, `color`, `label`  |
| container_scanning | Docker image vulnerability count | `link`, `color`, `label`  |
| dependency_scanning | Dependency vulnerability count | `link`, `color`, `label` |
| any other badge key | Custom badges | `link`, `color`, `label`, `message`, `script`, `logo`, `logocolor` |

By setting these sub-keys you can modify the depiction of a badge

| **Sub-key** | **Example** | **Value** |
| ------- | ----------- | ---- |
| color   | ![](https://img.shields.io/badge/label-message-lightblue.svg) <br> ![](https://img.shields.io/badge/label-message-red.svg) <br> ![](https://img.shields.io/badge/label-message-008800.svg) | lightblue <br> red <br> hex (without leading #) 008800 |
| label   | ![](https://img.shields.io/badge/example-5-008800.svg)  <br> ![](https://img.shields.io/badge/vulnerabilities-5-008800.svg)  | example <br> vulnerabilities |
| message   | ![](https://img.shields.io/badge/license-MIT-008800.svg)  <br> ![](https://img.shields.io/badge/license-Apache2-008800.svg) | MIT <br> Apache2 |
| logo   | ![](https://img.shields.io/badge/image-alpine-008800.svg?logo=docker&style=popout-square) <br> ![](https://img.shields.io/badge/issues-21-008800.svg?logo=gitlab&style=popout-square) | docker <br><br> gitlab |
| logocolor   | ![](https://img.shields.io/badge/image-alpine-008800.svg?logo=docker&style=popout-square&logoColor=lightblue)  <br> ![](https://img.shields.io/badge/image-alpine-008800.svg?logo=docker&style=popout-square&logoColor=007d7d) | lightblue <br><br> hex (without leading #) 007d7d |
| script  |  | Execute an arbitrary script. Output is used instead of `message`.<br>Make sure all necessary binaries are installed before the script is running |
| link   |  | Target link when clicking on the badge |