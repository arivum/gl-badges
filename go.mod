module arivum.de/gl-badges

go 1.12

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/xanzy/go-gitlab v0.19.0
	gopkg.in/yaml.v2 v2.2.2
)
