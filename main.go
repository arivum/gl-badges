package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"
)

// YamlObj Represents the Yaml File
type YamlObj struct {
	Settings BadgeSettings
	Badges   map[string]Badge
}

// BadgeSettings section from yaml
type BadgeSettings struct {
	Style string
}

// Badge struct in yaml file
type Badge struct {
	Color     string
	Link      string
	Image     string
	Script    string
	Message   string
	Label     string
	Logo      string
	Logocolor string
}

// GetEnv either returns Environment variable or a fallback value
func GetEnv(key string, fallback string) string {
	val, ok := os.LookupEnv(key)
	if !ok {
		return fallback
	}
	return val
}

// DeleteBadges first removes all badges from project
func DeleteBadges(client *gitlab.Client, project *gitlab.Project) {
	badges, _, err := client.ProjectBadges.ListProjectBadges(project.ID, nil)
	if err != nil {
		log.Error(err)
		log.Exit(1)
	}
	for _, badge := range badges {
		client.ProjectBadges.DeleteProjectBadge(project.ID, badge.ID, nil)
	}
}

// ParseYaml reads .gitlab-badges.yml
func ParseYaml() YamlObj {
	filename := ""
	if _, err := os.Stat(".gitlab-badges.yml"); !os.IsNotExist(err) {
		log.Info("Found: .gitlab-badges.yml")
		filename = ".gitlab-badges.yml"
	} else if _, err := os.Stat(".gitlab/badges.yml"); !os.IsNotExist(err) {
		log.Info("Found: .gitlab/badges.yml")
		filename = ".gitlab/badges.yml"
	}

	if filename == "" {
		log.Fatal("Not found: gitlab-badges yaml configuration file")
	}

	content, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Error(err)
		log.Exit(1)
	}
	var v YamlObj
	yaml.Unmarshal(content, &v)
	return v
}

// FillBadgeStruct is used to fill Badge instance with default values if attributes are empty
func FillBadgeStruct(value *Badge, filename string, label string, color string, link string) bool {

	if value.Color == "" {
		value.Color = color
	}
	if value.Label == "" {
		value.Label = label
	}
	if value.Link == "" {
		value.Link = link
	}

	jsonStr, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Warn("Couldn't find: ", filename)
		return false
	}
	v := make(map[string]interface{})
	err = json.Unmarshal(jsonStr, &v)
	if err != nil {
		log.Warn("Couldn't parse: JSON in ", filename)
		return false
	}
	value.Message = strconv.Itoa(len(v["vulnerabilities"].([]interface{})))
	return true
}

func main() {
	for _, key := range []string{"API_KEY", "PROJECT"} {
		if GetEnv(key, "") == "" {
			log.Error("Set: ", key, " env variable")
			log.Exit(1)
		}
	}

	yamlObj := ParseYaml()
	style := yamlObj.Settings.Style

	client := gitlab.NewClient(nil, GetEnv("API_KEY", ""))

	project, _, err := client.Projects.GetProject(GetEnv("PROJECT", ""), nil)
	if err != nil {
		log.Error(err)
		log.Exit(1)
	}

	DeleteBadges(client, project)

	for key, value := range yamlObj.Badges {
		switch key {
		case "pipeline":
			imageURL := "https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg?style=" + style
			if value.Link == "" {
				value.Link = "https://gitlab.com/%{project_path}/commits/%{default_branch}"
			}
			client.ProjectBadges.AddProjectBadge(project.ID, &gitlab.AddProjectBadgeOptions{
				LinkURL:  &value.Link,
				ImageURL: &imageURL,
			})
		case "coverage":
			imageURL := "https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg?style=" + style
			if value.Link == "" {
				value.Link = "https://gitlab.com/%{project_path}/commits/%{default_branch}"
			}
			client.ProjectBadges.AddProjectBadge(project.ID, &gitlab.AddProjectBadgeOptions{
				LinkURL:  &value.Link,
				ImageURL: &imageURL,
			})
		case "container_scanning":
			if !FillBadgeStruct(&value, "gl-container-scanning-report.json", "container vulnerabilities", "0093C3", "https://gitlab.com/%{project_path}/security/dashboard") {
				break
			}

			switch style {
			case "flat-square":
				style = "popout-square"
			default:
				style = "popout"
			}

			imageURL := fmt.Sprintf("https://img.shields.io/badge/%s-%s-%s.svg?style=%s&logo=docker&logoColor=0093C3", value.Label, value.Message, value.Color, style)
			client.ProjectBadges.AddProjectBadge(project.ID, &gitlab.AddProjectBadgeOptions{
				LinkURL:  &value.Link,
				ImageURL: &imageURL,
			})
		case "dependency_scanning":

			if !FillBadgeStruct(&value, "gl-dependency-scanning-report.json", "dependency vulnerabilities", "red", "https://gitlab.com/%{project_path}/dependencies") {
				break
			}

			imageURL := fmt.Sprintf("https://img.shields.io/badge/%s-%s-%s.svg?style=%s", value.Label, value.Message, value.Color, style)
			client.ProjectBadges.AddProjectBadge(project.ID, &gitlab.AddProjectBadgeOptions{
				LinkURL:  &value.Link,
				ImageURL: &imageURL,
			})
		case "sast":

			if !FillBadgeStruct(&value, "gl-sast-report.json", "sast vulnerabilities", "red", "https://gitlab.com/%{project_path}/security/dashboard") {
				break
			}

			imageURL := fmt.Sprintf("https://img.shields.io/badge/%s-%s-%s.svg?style=%s", value.Label, value.Message, value.Color, style)
			client.ProjectBadges.AddProjectBadge(project.ID, &gitlab.AddProjectBadgeOptions{
				LinkURL:  &value.Link,
				ImageURL: &imageURL,
			})
		case "dast":

			if !FillBadgeStruct(&value, "gl-dast-report.json", "dast vulnerabilities", "red", "https://gitlab.com/%{project_path}/security/dashboard") {
				break
			}

			imageURL := fmt.Sprintf("https://img.shields.io/badge/%s-%s-%s.svg?style=%s", value.Label, value.Message, value.Color, style)
			client.ProjectBadges.AddProjectBadge(project.ID, &gitlab.AddProjectBadgeOptions{
				LinkURL:  &value.Link,
				ImageURL: &imageURL,
			})
		default:
			furtherParams := fmt.Sprintf("?style=%s", style)
			if value.Color == "" {
				value.Color = "green"
			}
			if value.Link == "" {
				value.Link = "https://gitlab.com/%{project_path}"
			}
			if value.Label == "" {
				log.Error("No label found for ", value)
				break
			}
			if value.Logo != "" {
				if value.Logocolor == "" {
					value.Logocolor = "0093C3"
				}
				furtherParams = fmt.Sprintf("?style=popout-square&logo=%s&logoColor=%s", value.Logo, value.Logocolor)
			}
			if value.Message == "" && value.Script == "" {
				log.Error("No message or script found for ", value)
				break
			} else if value.Script != "" {
				out, err := exec.Command("bash", "-c", fmt.Sprintf(`%s`, value.Script)).Output()
				if err != nil {
					log.Error(err)
					break
				}
				value.Message = strings.Trim(string(out), "\n \t")
				imageURL := fmt.Sprintf("https://img.shields.io/badge/%s-%s-%s.svg%s", value.Label, value.Message, value.Color, furtherParams)
				client.ProjectBadges.AddProjectBadge(project.ID, &gitlab.AddProjectBadgeOptions{
					LinkURL:  &value.Link,
					ImageURL: &imageURL,
				})
			} else {
				imageURL := fmt.Sprintf("https://img.shields.io/badge/%s-%s-%s.svg%s", value.Label, value.Message, value.Color, furtherParams)
				client.ProjectBadges.AddProjectBadge(project.ID, &gitlab.AddProjectBadgeOptions{
					LinkURL:  &value.Link,
					ImageURL: &imageURL,
				})
			}
		}
	}

}
